# ����� �� ������ ������������ ������ "����������������� ������� �� ��������"

## ��������:

**���� ������:** - �������� ���������� ������ ������� � ����������������� ������� �� ������ ���������� ����������������.

## ����� ����������
����������� ����������������� ������� ����� ����������� ���������, �� ��� ������ �������� ���������� ������. ������� ���� ������������ ��� "������ ��������" � � ������������ ���� �������������� � ������� ������������ ������, ����������� �� ����� ���� ����� �������.�� ���� �������� �� ��, ��� ����� TVector ������ � TMatrix � ������� �������������, ��� ����������� ������������ ��� ������������� ���������������� ������������ �������.

## ��� ������� � ������:

### utmatrix.h

```c++
#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000;
```


### ������ �������
```c++
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // ������ �������
  int StartIndex; // ������ ������� �������� �������
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // ����������� �����������
  ~TVector();
  int GetSize()      { return Size;       } // ������ �������
  int GetStartIndex(){ return StartIndex; } // ������ ������� ��������
  ValType& operator[](int pos);             // ������
  bool operator==(const TVector &v) const;  // ���������
  bool operator!=(const TVector &v) const;  // ���������
  TVector& operator=(const TVector &v);     // ������������

  // ��������� ��������
  TVector  operator+(const ValType &val);   // ��������� ������
  TVector  operator-(const ValType &val);   // ������� ������
  TVector  operator*(const ValType &val);   // �������� �� ������

  // ��������� ��������
  TVector  operator+(const TVector &v);     // ��������
  TVector  operator-(const TVector &v);     // ���������
  ValType  operator*(const TVector &v);     // ��������� ������������

  // ����-�����
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};
```
### ������������ � ���������� �������
```c++
template <class ValType>
TVector<ValType>::TVector(int s, int si):Size(s),StartIndex(si)
{
	if((s>MAX_VECTOR_SIZE)||(s<0)) throw 1;
	else if((si>MAX_VECTOR_SIZE)||(si<0)) throw 1;
	else
	pVector = new ValType [s];
} /*-------------------------------------------------------------------------*/

template <class ValType> //����������� �����������
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size=v.Size;
	StartIndex=v.StartIndex;
	pVector = new ValType[Size];
	for (int i=0;i<Size;i++){pVector[i]=v.pVector[i];}
} /*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete [] pVector;
} /*-------------------------------------------------------------------------*/
```

### ���������� ����������
```c++
template <class ValType> // ������
ValType& TVector<ValType>::operator[](int pos)
{
	if (pos < StartIndex || pos >= StartIndex + Size) {
		throw 3;
	}
	return pVector[pos-StartIndex];
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator==(const TVector &v) const
{
	if(Size!=v.Size)return false;
	else if(StartIndex!=v.StartIndex) return false;
	else {
		for (int i=0;i<Size;i++){
			if (pVector[i]!=v.pVector[i]){
				return false;
			}
		}

	return true;
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator!=(const TVector &v) const
{
	if(Size!=v.Size)return true;
	else if(StartIndex!=v.StartIndex) return true;
	else {
		for (int i=0;i<Size;i++){
			if (pVector[i]!=v.pVector[i]){
				return true;
			}
		}

	return false;
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������������
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if(&v!=this){
		Size=v.Size;
		StartIndex = v.StartIndex;
		
		delete [] pVector;
		pVector = new ValType [Size];

		for (int i=0;i<Size;i++) pVector[i]=v.pVector[i];
	}
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector <ValType> buf(Size,StartIndex);
	
	for(int i=0;i<Size;i++){
		buf.pVector[i]=pVector[i]+val;}

	return buf;

} /*-------------------------------------------------------------------------*/

template <class ValType> // ������� ������
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector <ValType> buf(Size,StartIndex);
	
	for(int i=0;i<Size;i++){
		buf.pVector[i]=pVector[i]-val;}

	return buf;
} /*-------------------------------------------------------------------------*/

template <class ValType> // �������� �� ������
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector <ValType> buf(Size,StartIndex);
	
	for(int i=0;i<Size;i++) buf.pVector[i]=pVector[i]*val;

	return buf;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] + v.pVector[i];
	}

	return temp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	TVector<ValType> temp(Size, StartIndex);

	for (int i = 0; i < Size; i++) {
		temp.pVector[i] = pVector[i] - v.pVector[i];
	}

	return temp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������������
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if (Size != v.Size || StartIndex != v.StartIndex) {
		throw 4;
	}

	ValType skalar = pVector[0]*v.pVector[0];
	for (int i=1;i<Size;i++){
		skalar+=(pVector[i]*v.pVector[i]);
	}
	return skalar;
} /*-------------------------------------------------------------------------*/
```

### ����������������� �������

```c++
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // �����������
  TMatrix(const TVector<TVector<ValType> > &mt); // �������������� ����
  bool operator==(const TMatrix &mt) const;      // ���������
  bool operator!=(const TMatrix &mt) const;      // ���������
  TMatrix& operator= (const TMatrix &mt);        // ������������
  TMatrix  operator+ (const TMatrix &mt);        // ��������
  TMatrix  operator- (const TMatrix &mt);        // ���������

  // ���� / �����
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};
```
### ������������
**���������� �� �����, ��� ��� ������ ����� ������������� ���������, ��-�� ����, ��� ������� - ������������ ������ �� ��������**
```c++
template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s > MAX_MATRIX_SIZE) {
		throw 5;
	}

	for (int i = 0; i < s; i++) {
		pVector[i] = TVector<ValType>(s - i, i);
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // ����������� �����������
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) {}

template <class ValType> // ����������� �������������� ����
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) {}
### ���������� ����������
**��������� ���� ����� ����������� ������ ��������� ��� ������������� ��������� ������ ������ �������� ������������ ��� ����������������� �������**
template <class ValType> // ���������
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator==(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������������
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	TVector<TVector<ValType> >::operator=(mt);
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator-(mt);
} /*-------------------------------------------------------------------------*/

// TVector �3 �2 �4 �6
// TMatrix �2 �2 �3 �3
#endif
```

## �����
**����� �������� ��� ������, ���������� ��� �������� �������**
```c++
#include "utmatrix.h"

#include <gtest.h>
```
### ����� ��� �������
```c++

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
  TVector<int> v(10);
  TVector<int> v2=v;
  EXPECT_EQ(v2,v);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
  TVector<int> v(10);
  TVector<int> v2=v;
  v2[4]=333;
  EXPECT_NE(v2,v);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
  TVector<int> v(4);
  ASSERT_ANY_THROW(v[-2]=0);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
  TVector<int> v(4);
  ASSERT_ANY_THROW(v[7]=0);
}

TEST(TVector, can_assign_vector_to_itself)
{
  TVector<int> v(2);
  v[0]=1;
  v[1]=2;
  v=v;
  EXPECT_EQ(v[0],1);
  EXPECT_EQ(v[1],2);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
  TVector<int> v1(2),v2(2);
  v1[0]=1;
  v1[1]=2;
  v2=v1;
  EXPECT_EQ(v2[0],1);
  EXPECT_EQ(v2[1],2);
}

TEST(TVector, assign_operator_change_vector_size)
{
  TVector<int> v1(3),v2(2);
  v1[0]=1;
  v1[1]=2;
  v2=v1;
  EXPECT_EQ(v2.GetSize(),3);
}

TEST(TVector, can_assign_vectors_of_different_size)
{
  TVector<int> v1(2),v2(5);
  v1[0]=1;
  v1[1]=2;
  v2=v1;
  EXPECT_EQ(v2[0],1);
  EXPECT_EQ(v2[1],2);
}

TEST(TVector, compare_equal_vectors_return_true)
{
  TVector<int> v1(2),v2(2);
  v1[0]=1;
  v1[1]=2;
  v2=v1;
  EXPECT_TRUE(v2==v1);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v1(2);
  EXPECT_TRUE(v1==v1);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
  TVector<int> v1(2),v2(3);
  EXPECT_FALSE(v2==v1);
}

TEST(TVector, can_add_scalar_to_vector)
{
  TVector<int> v1(2),v2(2);
	v1[0]=0;v1[1]=0;	
	v2=v1+7;
	EXPECT_EQ(v2[0],7);
	EXPECT_EQ(v2[1],7);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
  TVector<int> v1(2),v2(2);
	v1[0]=0;v1[1]=0;	
	v2=v1-7;
	EXPECT_EQ(v2[0],-7);
	EXPECT_EQ(v2[1],-7);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
  TVector<int> v1(2),v2(2);
	v1[0]=1;v1[1]=1;	
	v2=v1*7;
	EXPECT_EQ(v2[0],7);
	EXPECT_EQ(v2[1],7);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
  TVector<int> v1(3),v2(3),v(3);
	v1[0]=3;	v1[1]=2;	v1[2]=1;
	v2[0]=3;	v2[1]=2;	v2[2]=3;
	v=v1+v2;
	EXPECT_EQ(v[0],6);
	EXPECT_EQ(v[1],4);
	EXPECT_EQ(v[2],4);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
  TVector<int> v1(3),v2(4),v(3);
	v1[0]=3;	v1[1]=2;	v1[2]=1;
	v2[0]=3;	v2[1]=2;	v2[2]=3;	v2[3]=1;
	ASSERT_ANY_THROW( v=v1+v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
   TVector<int> v1(3),v2(3),v(3);
	v1[0]=3;	v1[1]=2;	v1[2]=1;
	v2[0]=3;	v2[1]=1;	v2[2]=3;
	v=v1-v2;
	EXPECT_EQ(v[0],0);
	EXPECT_EQ(v[1],1);
	EXPECT_EQ(v[2],-2);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
  TVector<int> v1(3),v2(4),v(3);
	v1[0]=3;	v1[1]=2;	v1[2]=1;
	v2[0]=3;	v2[1]=2;	v2[2]=3;	v2[3]=1;
	ASSERT_ANY_THROW( v=v1-v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
  TVector<int> v1(3),v2(3);
  int v;
	v1[0]=3;	v1[1]=2;	v1[2]=2;
	v2[0]=3;	v2[1]=1;	v2[2]=3;
	v=v1*v2;
	EXPECT_EQ(v,17);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
  TVector<int> v1(3),v2(4),v(3);
	v1[0]=3;	v1[1]=2;	v1[2]=1;
	v2[0]=3;	v2[1]=2;	v2[2]=3;	v2[3]=1;
	ASSERT_ANY_THROW( v=v1*v2);
}
```

## ����� ��� �������
```c++
#include "utmatrix.h"

#include <gtest.h>

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> m(5);
	TMatrix<int> c = m;

	EXPECT_EQ(m, c);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> m(5);
	TMatrix<int> c = m;

	c[1][1]=5;

	EXPECT_NE(m,c);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<int> m(4);

	EXPECT_EQ(m.GetSize(), 4);
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> m(5);

	m[1][1]=5;

	EXPECT_EQ(m[1][1],5);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> m(5);

	
	ASSERT_ANY_THROW(m[-1][1]=5;);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
  TMatrix<int> m(5);
  ASSERT_ANY_THROW(m[5][5]=5;);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
  TMatrix<int> m(5);
  m[2][2]=1;
  m[1][1]=2;

  m=m;
  EXPECT_EQ(m[2][2],1);
  EXPECT_EQ(m[1][1],2);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
  TMatrix<int> m(5),m2(5);
  m[2][2]=1;
  m[1][1]=2;

  m2=m;
  EXPECT_EQ(m2[2][2],1);
  EXPECT_EQ(m2[1][1],2);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
  TMatrix<int> m(5),m2(10);
  m2=m;
  EXPECT_EQ(m2.GetSize(),5);
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
  TMatrix<int> m(5),m2(7);
  m[2][2]=1;
  m[1][1]=2;

  m2=m;
  EXPECT_EQ(m2[2][2],1);
  EXPECT_EQ(m2[1][1],2);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
  TMatrix<int> m(5),m2(7);
  m[2][2]=1;
  m[1][1]=2;

  m2=m;
  EXPECT_TRUE(m2==m);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
  TMatrix<int> m(5);

  EXPECT_TRUE(m==m);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
  TMatrix<int> m1(2), m2(3);

  EXPECT_FALSE(m1 == m2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
  TMatrix<int> m1(2), m2(2);
  m1[0][0]=3; m1[1][1]=3;m1[0][1]=2;
  m2[1][1]=5; m2[0][0]=5;m2[0][1]=2;
  TMatrix<int> m3(3);
  m3=m2+m1;

  EXPECT_EQ(m3[0][0],8);
  EXPECT_EQ(m3[1][1],8);
  EXPECT_EQ(m3[0][1],4);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
  TMatrix<int> m1(3), m2(2);
  m1[0][0]=3; m1[1][1]=3;m1[0][1]=2;
  m2[1][1]=5; m2[0][0]=5;m2[0][1]=2;
  ASSERT_ANY_THROW( m2+m1);

}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
  TMatrix<int> m1(2), m2(2);
  m1[0][0]=3; m1[1][1]=3;m1[0][1]=2;
  m2[1][1]=5; m2[0][0]=5;m2[0][1]=2;
  TMatrix<int> m3(3);
  m3=m2-m1;

  EXPECT_EQ(m3[0][0],2);
  EXPECT_EQ(m3[1][1],2);
  EXPECT_EQ(m3[0][1],0);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
  TMatrix<int> m1(3), m2(2);
  m1[0][0]=3; m1[1][1]=3;m1[0][1]=2;
  m2[1][1]=5; m2[0][0]=5;m2[0][1]=2;
  ASSERT_ANY_THROW( m2-m1);
}
```

## �����

**������ ���� ����������� �������� �������, ����������� � �������� �������.**

